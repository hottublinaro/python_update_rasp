import time
from flask import Flask
import signal
import git 
from flask_cors import CORS
import os
import threading

app = Flask(__name__)
CORS(app)



@app.route('/<data>', methods=['GET'])
def home(data):
    if data == "update":
        print('2024')
        #อัพเดทโค้ดเว็บ
        web = git.cmd.Git('/var/www/rasp_new_logic')
        web.pull()
        time.sleep(10)
        #อัพเดทโค้ด hottub python
        machine = git.cmd.Git('/home/pi/hottub_ma')
        machine.pull()
        time.sleep(10)
        #server python
        machine_server = git.cmd.Git('/home/pi/hottub_server_rasp')
        machine_server.pull()
        time.sleep(10)
        #logger
        machine_logger = git.cmd.Git('/home/pi/logger_rasp')
        machine_logger.pull()
        time.sleep(10)
        #logger volt
        machine_logger_volt = git.cmd.Git('/home/pi/logger_volt_rasp')
        machine_logger_volt.pull()
        time.sleep(10)

        cmd = 'sudo reboot'
        os.system(cmd)

    return data

def run_loop():
    while True:
        try:
            path_file_update = '/home/pi/txt_file/old_version_update.txt'
            status_path_exit = os.path.isfile(path_file_update)
            if status_path_exit == False:
                with open('/home/pi/txt_file/old_version_update.txt','w') as write_status_update_online:
                    write_status_update_online.write('0')
                    write_status_update_online.close()
                    time.sleep(1)
                    machine_server = git.cmd.Git('/home/pi/hottub_server_rasp')
                    machine_server.pull()
                    time.sleep(10)
                    os.system('sudo chmod -R 777 /home/pi/txt_file')
                    time.sleep(1)
                    os.system('sudo reboot')
            else:
                with open('/home/pi/txt_file/old_version_update.txt','r') as read_version_old:
                    version_old = read_version_old.readline().strip()
                    read_version_old.close()
                if str(version_old) == "0" :
                    machine_logger = git.cmd.Git('/home/pi/logger_rasp')
                    machine_logger.pull()
                    time.sleep(10)
                    #logger volt
                    machine_logger_volt = git.cmd.Git('/home/pi/logger_volt_rasp')
                    machine_logger_volt.pull()
                    time.sleep(10)
                    with open('/home/pi/txt_file/old_version_update.txt','w') as write_status_update_online:
                        write_status_update_online.write('1')
                        write_status_update_online.close()
                    time.sleep(1)
                    os.system('sudo reboot')
                elif str(version_old) == "1" : 
                    web = git.cmd.Git('/var/www/rasp_new_logic')
                    web.pull()
                    time.sleep(10)
                    #server python
                    machine_server = git.cmd.Git('/home/pi/hottub_server_rasp')
                    machine_server.pull()
                    time.sleep(10)
                    with open('/home/pi/txt_file/old_version_update.txt','w') as write_status_update_online:
                        write_status_update_online.write('2')
                        write_status_update_online.close()
                    time.sleep(1)
                    os.system('sudo systemctl restart hottub_server.service')
                    time.sleep(1)
                    os.system('sudo systemctl restart apache2.service')
                elif str(version_old) == "2" :
                    path_plc_in_16 = '/home/pi/txt_file/status_plc_in_16.txt'
                    status_path_plc_in_16 = os.path.isfile(path_plc_in_16)
                    if status_path_plc_in_16  == False :
                        with open('/home/pi/txt_file/status_plc_in_16.txt','w') as write_status_plc_in_16:
                            write_status_plc_in_16.write(str(0))
                            write_status_plc_in_16.close()

                    time.sleep(1)
                    os.system('sudo chmod -R 777 /home/pi/txt_file')
                    time.sleep(1)
                    #web service
                    web = git.cmd.Git('/var/www/rasp_new_logic')
                    web.pull()
                    time.sleep(10)
                    #อัพเดทโค้ด hottub python
                    machine = git.cmd.Git('/home/pi/hottub_ma')
                    machine.pull()
                    time.sleep(10)
                    #server python
                    machine_server = git.cmd.Git('/home/pi/hottub_server_rasp')
                    machine_server.pull()
                    time.sleep(10)

                    os.system('sudo systemctl restart led-blink.service')
                    time.sleep(1)
                    os.system('sudo systemctl restart hottub_server.service')
                    time.sleep(1)

                    with open('/home/pi/txt_file/old_version_update.txt','w') as write_status_update_online:
                        write_status_update_online.write('3')
                        write_status_update_online.close()

                elif str(version_old) == "3":
                    #update pH ORP work parallial in hour
                    path_aco = '/home/pi/txt_file/counter_aco.txt'
                    status_path_aco = os.path.isfile(path_aco)
                    if status_path_aco  == False :
                        with open('/home/pi/txt_file/counter_aco.txt','w') as write_status_aco:
                            write_status_aco.write(str(0))
                            write_status_aco.close()
                    time.sleep(1)

                    path_apf = '/home/pi/txt_file/counter_apf.txt'
                    status_path_apf = os.path.isfile(path_apf)
                    if status_path_apf  == False :
                        with open('/home/pi/txt_file/counter_apf.txt','w') as write_status_apf:
                            write_status_apf.write(str(0))
                            write_status_apf.close()
                    time.sleep(1)

                    path_chlorine = '/home/pi/txt_file/counter_chlorine.txt'
                    status_path_chlorine = os.path.isfile(path_chlorine)
                    if status_path_chlorine  == False :
                        with open('/home/pi/txt_file/counter_chlorine.txt','w') as write_status_chlorine:
                            write_status_chlorine.write(str(0))
                            write_status_chlorine.close()
                    time.sleep(1)

                    os.system('sudo chmod -R 777 /home/pi/txt_file')
                    time.sleep(1)
                    #web service
                    web = git.cmd.Git('/var/www/rasp_new_logic')
                    web.pull()
                    time.sleep(10)

                    machine = git.cmd.Git('/home/pi/hottub_ma')
                    machine.pull()
                    time.sleep(15)

                    os.system('sudo systemctl restart led-blink.service')
                    time.sleep(1)
                    os.system('sudo systemctl restart apache2')
                    time.sleep(1)

                    with open('/home/pi/txt_file/old_version_update.txt','w') as write_status_update_online:
                        write_status_update_online.write('4')
                        write_status_update_online.close()
            
                elif str(version_old) == "4":
                    machine = git.cmd.Git('/home/pi/hottub_ma')
                    machine.pull()
                    time.sleep(15)

                    os.system('sudo systemctl restart led-blink.service')
                    time.sleep(1)
                    
                    with open('/home/pi/txt_file/old_version_update.txt','w') as write_status_update_online:
                        write_status_update_online.write('5')
                        write_status_update_online.close()

                elif str(version_old) == "5":
                    machine = git.cmd.Git('/home/pi/hottub_ma')
                    machine.pull()
                    time.sleep(15)

                    os.system('sudo systemctl restart led-blink.service')
                    time.sleep(1)
                    
                    with open('/home/pi/txt_file/old_version_update.txt','w') as write_status_update_online:
                        write_status_update_online.write('6')
                        write_status_update_online.close()

            time.sleep(5)
        except:
            pass


if __name__ =="__main__":
    # signal.signal(signal.SIGTERM, run_loop)
    threading.Thread(target=run_loop, daemon=True).start()
    app.run(debug=True, port=5006, host='0.0.0.0', threaded=True)
   
  